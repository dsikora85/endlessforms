﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour 
{
	private SpriteRenderer tileSprite;
	private float normalAlpha = 0.525f;
	private float onOverColor = 1.0f;

	private bool blocked;
	public bool IsBlocked
	{
		get { return blocked; }
	}

	int row;
	public int Row
	{
		get { return row; }
	}

	int col;
	public int Col
	{
		get { return col; }
	}

	public bool IsEmpty
	{
		get { return farmItem == null; }
	}

	private FarmItem farmItem;
	public FarmItem FarmItem
	{
		get { return farmItem; }
	}

	public Tile(int x, int y)
	{
		row = y;
		col = x;
		blocked = false;
	}

	public void Reset(int x, int y)
	{
		farmItem = null;
		row = x;
		col = y;
	}

	public void Clear()
	{
		blocked = false;
		farmItem = null;

		for(int i =0; i<transform.childCount; i++)
		{
			Destroy(transform.GetChild(i).gameObject);
		}
	}

	public void SetBlocked()
	{
		int blockedObjId = Random.Range(0, GameManager.Instance.Stockroom.Blocked.Length);
		SetBlocked(blockedObjId);
	}

	private void SetBlocked(int blockedID)
	{
		GameObject block = Instantiate<GameObject>(GameManager.Instance.Stockroom.Blocked[blockedID]);
		block.transform.SetParent(transform, true);
		block.transform.localPosition = Vector3.zero;

		blocked = true;
	}

	public void SetFarmItem(FarmItem item)
	{
		farmItem = item;
	}

	public int SaveGet()
	{
		if (IsBlocked)
		{
			BlockingObject block = transform.GetChild(0).GetComponent<BlockingObject>();
			switch (block.Type)
			{
				case Stockroom.EFarmItem.BlockedHay1:
					return -2;
				case Stockroom.EFarmItem.BlockedHay2:
					return -3;
				case Stockroom.EFarmItem.BlockedStub1:
					return -4;
				case Stockroom.EFarmItem.BlockedStub2:
					return -5;
				case Stockroom.EFarmItem.BlockedCrate:
					return -6;
				default:
					return -2;
			}
		}
		else if (IsEmpty)
		{
			return -1;
		}
		else
		{
			return GetComponentInChildren<FarmItem>().SaveGet();
		}
	}

	public void LoadSet(int data)
	{
		Clear();

		if (data < -1)
		{
			SetBlocked(-data - 2);
		}
		else if(data >= 0)
		{
			farmItem = GameManager.Instance.Stockroom.GetItemByType((Stockroom.EFarmItem)data);

			farmItem.transform.SetParent(transform, true);
			farmItem.transform.localPosition = Vector3.zero;

			farmItem.PutOnBoard();
		}
	}
}
