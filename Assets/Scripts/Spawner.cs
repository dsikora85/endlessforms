﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour 
{
	public GameObject Form;
	public int ElemID;

	GameObject currentSpawnElement;

	// Use this for initialization
	void Start () 
	{
		Spawn();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void Spawn()
	{
		if (transform.childCount > 0)
			return;

		FarmItem farmItem = GameManager.Instance.Stockroom.GetItemWithProbability();

		currentSpawnElement = Instantiate(farmItem.gameObject, Vector3.zero, Quaternion.identity) as GameObject;
		currentSpawnElement.name = "obj1";
		currentSpawnElement.transform.SetParent(transform, true);
		currentSpawnElement.transform.localPosition = Vector3.zero;

		farmItem.Spawn();
	}

	public void Show()
	{
		FarmItem item = currentSpawnElement.GetComponent<FarmItem>();
		item.Show();
	}

	public void Hide()
	{
		FarmItem item = currentSpawnElement.GetComponent<FarmItem>();
		item.Hide();
	}

	public void Save()
	{
		PlayerPrefs.SetInt("Spawner", (int)currentSpawnElement.GetComponent<FarmItem>().ItemType);
	}

	public void Load()
	{
		if(transform.childCount > 0)
			Destroy(transform.GetChild(0).gameObject);
		
		Stockroom.EFarmItem itemType = (Stockroom.EFarmItem)PlayerPrefs.GetInt("Spawner");
		FarmItem farmItem = GameManager.Instance.Stockroom.GetItemByType(itemType);

		currentSpawnElement = farmItem.gameObject;
		currentSpawnElement.name = "obj1";
		currentSpawnElement.transform.SetParent(transform, true);
		currentSpawnElement.transform.localPosition = Vector3.zero;

		farmItem.Spawn();
	}

	public void Reset()
	{
		if (transform.childCount > 0)
			Destroy(transform.GetChild(0).gameObject);

		Spawn();
	}
}
