﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DragAndDrop : MonoBehaviour 
{
	private bool _mouseState;
	private GameObject target;
	public Vector3 screenSpace;
	public Vector3 offset;
	public LayerMask raycastLayer;

	private Vector3 screenPoint;
	private Vector3 startPosition;
	private Tile currentTile;

	// Use this for initialization
	void Start()
	{
		startPosition = transform.position;
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			target = GetClickedObject(Camera.main.ScreenToWorldPoint(Input.mousePosition));
			if (target != null)
			{
				_mouseState = true;
				screenSpace = Camera.main.WorldToScreenPoint(target.transform.position);
				offset = target.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z));
			}
		}
		if (Input.GetMouseButtonUp(0))
		{
			if (_mouseState)
			{
				_mouseState = false;
				if (Board.Instance.TryPutObjectOnBoard(target) == false)
				{ 
					DOTween.To(() => target.transform.localPosition, pos => target.transform.localPosition = pos, Vector3.zero, 0.25f).SetEase(Ease.OutCubic);
				}
			} 
			else
			{
				var curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);

				//convert the screen mouse position to world point and adjust with offset
				var curPosition = Camera.main.ScreenToWorldPoint(curScreenSpace) + offset;

				Board.Instance.TryRemoveObject(curPosition);
			}

			currentTile = null;
		}
		if (_mouseState)
		{
			//keep track of the mouse position
			var curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);

			//convert the screen mouse position to world point and adjust with offset
			var curPosition = Camera.main.ScreenToWorldPoint(curScreenSpace) + offset;

			//update the position of the object in the world
			target.transform.position = curPosition;

			Board.Instance.UpdateIndicator(target);
		}
	}


	GameObject GetClickedObject(Vector3 pos)
	{
		GameObject target = null;
		RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.up, 1.0f, raycastLayer);
		if (hit)  
		{
			target = hit.collider.gameObject;
			Tile tile = hit.collider.GetComponent<Tile>();
			if (tile != null)
				target = null;
		}

		return target;
	}
}
