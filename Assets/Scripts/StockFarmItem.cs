﻿using UnityEngine;
using System.Collections;

public class StockFarmItem : MonoBehaviour
{
	public FarmItem FarmItem;
	private int amount = 0;
	public int Amount
	{
		get { return amount; }
	}

	public bool Unlocked = true;
	public float Probability = 0.1f;

	public void IncreaseAmount(int val)
	{
		amount += val;
	}

	public void DecreaseAmount(int val)
	{
		amount -= val;
	}

	public void Save()
	{
		Debug.Log(FarmItem.ItemType.ToString() + " - " + amount);
		PlayerPrefs.SetInt(FarmItem.ItemType.ToString(), amount);
	}

	public void Load()
	{
		amount = PlayerPrefs.GetInt(FarmItem.ItemType.ToString(), 0);
	}

	public void Reset()
	{
		amount = 0;
	}
}
