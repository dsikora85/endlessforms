﻿using UnityEngine;
using System.Collections;

public class BoardIndicator : MonoBehaviour
{
	public SpriteRenderer RowIndicator;
	public SpriteRenderer ColIndicator;
	public Color NormalColor;
	public Color OccupiedColor;

	public void ShowIndicator (int row, int col, bool occupied)
	{
		ColIndicator.gameObject.SetActive(true);
		RowIndicator.gameObject.SetActive(true);

		float posX = -2.15f + (0.86f) * col;
		float posY = 2.9f - (0.82f) * row;

		Vector2 pos = ColIndicator.transform.position;
		pos.x = posX;
		ColIndicator.transform.position = pos;

		pos = RowIndicator.transform.position;
		pos.y = posY;
		RowIndicator.transform.position = pos;

		if (occupied)
		{
			ColIndicator.color = OccupiedColor;
			RowIndicator.color = OccupiedColor;
		}
		else
		{
			ColIndicator.color = NormalColor;
			RowIndicator.color = NormalColor;
		}
	}

	public void HideIndicator()
	{
		ColIndicator.gameObject.SetActive(false);
		RowIndicator.gameObject.SetActive(false);
	}
}
