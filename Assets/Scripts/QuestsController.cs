﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class QuestsController : MonoBehaviour
{
	private static GameManager instance;

	public ArrayList Quests = new ArrayList();

	void Start()
	{
		CreateNewQuest(QuestScript.QuestDifficulty.Easy);
		CreateNewQuest(QuestScript.QuestDifficulty.Easy);
		CreateNewQuest(QuestScript.QuestDifficulty.Medium);
		CreateNewQuest(QuestScript.QuestDifficulty.Hard);
	}

	public void CreateNewQuest(QuestScript.QuestDifficulty difficulty)
	{
		QuestScript quest = new QuestScript();
		quest.Create(difficulty);

		Quests.Add(quest);
	}

	public void QuestCompleted(QuestScript quest)
	{
		GameManager.Instance.Stockroom.AddCoins(quest.Reward);
		Board.Instance.HUD.UpdateCoins();
		Quests.Remove(quest);

		//TODO
		//remove items from stockroom
	}

	public void Save()
	{
		var json = new JSONClass();
		foreach (QuestScript quest in Quests)
			quest.Save(json);

		PlayerPrefs.SetString("Quests", json.ToString());
	}

	public void Load()
	{
		Quests.Clear();
		JSONNode json = JSONNode.Parse(PlayerPrefs.GetString("Quests"));

		JSONArray questsArray = json["Quests"].AsArray;
		for(int i=0; i<questsArray.Count; i++)
		{
			QuestScript quest = new QuestScript();
			quest.Load(questsArray[i]);
			Quests.Add(quest);
		}
	}

	public void Reset()
	{
		Quests.Clear();

		CreateNewQuest(QuestScript.QuestDifficulty.Easy);
		CreateNewQuest(QuestScript.QuestDifficulty.Easy);
		CreateNewQuest(QuestScript.QuestDifficulty.Medium);
		CreateNewQuest(QuestScript.QuestDifficulty.Hard);
	}
}
