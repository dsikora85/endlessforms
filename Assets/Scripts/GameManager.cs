﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	private static GameManager instance;

	[SerializeField]
	private Stockroom stockroom;
	public Stockroom Stockroom
	{
		get { return stockroom;  }
	}

	[SerializeField]
	private Spawner spawner;
	public Spawner Spawner
	{
		get { return spawner; }
	}

	private QuestsController questController;
	public QuestsController QuestController
	{
		get { return questController; }
	}

	// Construct  
	private GameManager()
	{

	}

	//  Instance  
	public static GameManager Instance
	{
		get
		{
			if (instance == null)
				instance = GameObject.FindObjectOfType(typeof(GameManager)) as GameManager;
			return instance;
		}
	}

	// Use this for initialization
	void Start () 
	{
		questController = GetComponent<QuestsController>();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
