﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PopupScreen : MonoBehaviour
{
	public Text Message;
	public Image PopUpImage;

	System.Action buttonOkAction;
	System.Action buttonNoAction;
	int price;

	public void Show(string message, System.Action button1Action = null, System.Action button2Action = null)
	{
		PopUpImage.gameObject.SetActive(true);

		Message.text = message;
		buttonOkAction = button1Action;
		buttonNoAction = button2Action;
		price = 0;
	}
	
	public void OnButtonOk()
	{
		if (GameManager.Instance.Stockroom.CoinsAmount >= price)
		{
			PopUpImage.gameObject.SetActive(false);

			if (buttonOkAction != null)
				buttonOkAction();
		}
	}

	public void OnButtonNo()
	{
		PopUpImage.gameObject.SetActive(false);

		if(buttonNoAction != null)
			buttonNoAction();
	}
}
