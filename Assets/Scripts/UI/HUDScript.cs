﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDScript : MonoBehaviour
{
	public StockroomUI StockroomPopup;
	public QuestBoardUI QuestBoardPopup;
	public SettingsUI SettingsPanel;
	public Text CoinsAmountText;
	public PopupScreen PopUp;

	void Start()
	{
		UpdateCoins();
	}

	public void OpenStockroomPopup()
	{
		if (Board.Instance.IsInteractive)
		{
			StockroomPopup.gameObject.SetActive(true);
			StockroomPopup.OnShow();
		}
	}

	public void OpenBoardPopup()
	{
		if (Board.Instance.IsInteractive)
		{
			QuestBoardPopup.gameObject.SetActive(true);
			QuestBoardPopup.OnShow();
		}
	}

	public void OpenSettings()
	{
		SettingsPanel.gameObject.SetActive(true);
	}

	public void UpdateCoins()
	{
		CoinsAmountText.text = GameManager.Instance.Stockroom.CoinsAmount.ToString();
	}

	public void ShowRemoveConfirmPopUp(System.Action onRemove = null)
	{
		PopUp.Show("Remove this object?", onRemove);
	}

	public void HidePopUp()
	{

	}
}
