﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class QuestUI : MonoBehaviour
{
	public GameObject QuestItemTemplate;
	public Text RewardText;
	public Button SendButton;

	private VerticalLayoutGroup layout;
	private QuestScript questRef;

	public void Setup(QuestScript quest)
	{
		questRef = quest;
		layout = GetComponentInChildren<VerticalLayoutGroup>();

		foreach(KeyValuePair<Stockroom.EFarmItem, QuestScript.QuestItem> questItem in quest.QuestItems)
		{
			GameObject questItemUI = Instantiate<GameObject>(QuestItemTemplate);
			questItemUI.GetComponent<QuestItemUI>().Setup(questItem.Value);

			questItemUI.gameObject.transform.SetParent(layout.gameObject.transform, false);
		}

		int emptyObj = 3 - quest.QuestItems.Count;
		for(int i=0; i<emptyObj; i++)
		{
			GameObject questItemUI = Instantiate<GameObject>(QuestItemTemplate);
			questItemUI.GetComponent<QuestItemUI>().SetupEmpty();

			questItemUI.gameObject.transform.SetParent(layout.gameObject.transform, false);
		}
		RewardText.text = quest.Reward.ToString();

		if (!quest.IsCompleted())
			SendButton.gameObject.SetActive(false);
	}
	
	public void OnSend()
	{
		GameManager.Instance.QuestController.QuestCompleted(questRef);
		Destroy(gameObject);
	}
}
