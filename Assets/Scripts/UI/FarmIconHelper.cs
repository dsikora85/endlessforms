﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FarmIconHelper : MonoBehaviour
{
	public Sprite[] Icons;
	static public Dictionary<Stockroom.EFarmItem, Sprite> IconsDictionary = new Dictionary<Stockroom.EFarmItem, Sprite>();

	void Start()
	{
		IconsDictionary.Add(Stockroom.EFarmItem.Potato, Icons[0]);
		IconsDictionary.Add(Stockroom.EFarmItem.Carrot, Icons[1]);
		IconsDictionary.Add(Stockroom.EFarmItem.Corn, Icons[2]);
		IconsDictionary.Add(Stockroom.EFarmItem.Radish, Icons[3]);
		IconsDictionary.Add(Stockroom.EFarmItem.Tomato, Icons[6]);
		IconsDictionary.Add(Stockroom.EFarmItem.Bamboo, Icons[9]);
		IconsDictionary.Add(Stockroom.EFarmItem.Cabbage, Icons[10]);
		IconsDictionary.Add(Stockroom.EFarmItem.CauliFlower, Icons[11]);
		IconsDictionary.Add(Stockroom.EFarmItem.Eggplant, Icons[12]);
		IconsDictionary.Add(Stockroom.EFarmItem.Grapes, Icons[13]);
		IconsDictionary.Add(Stockroom.EFarmItem.Mushroom, Icons[14]);
		IconsDictionary.Add(Stockroom.EFarmItem.Leek, Icons[15]);
		IconsDictionary.Add(Stockroom.EFarmItem.Pumpkin, Icons[16]);
		IconsDictionary.Add(Stockroom.EFarmItem.Wheat, Icons[17]);
	}

}
