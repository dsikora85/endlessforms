﻿using UnityEngine;
using System.Collections;

public class StockroomUI : MonoBehaviour
{
	public void OnShow()
	{
		StockroomItemUI[] elements = GetComponentsInChildren<StockroomItemUI>();

		foreach (StockroomItemUI elem in elements)
			elem.OnShow();
	}

	public void OnClose()
	{
		//Debug.Log(name);
		gameObject.SetActive(false);
	}

	public void UnlockLvl1()
	{
		GameManager.Instance.Stockroom.UnlockLevel1Items();
		OnShow();
	}

	public void UnlockLvl2()
	{
		GameManager.Instance.Stockroom.UnlockLevel2Items();
		OnShow();
	}
}
