﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class QuestItemUI : MonoBehaviour
{
	public Text Amount;
	public Image Icon;
	public Toggle Toggle;

	public void Setup(QuestScript.QuestItem questItem)
	{
		Amount.text = "x" + questItem.Amount;
		Icon.sprite = FarmIconHelper.IconsDictionary[questItem.ItemType];

		if (GameManager.Instance.Stockroom.GetStockItemByType(questItem.ItemType).Amount >= questItem.Amount)
			Toggle.isOn = true;
		else
			Toggle.isOn = false;
	}


	public void SetupEmpty()
	{
		Icon.gameObject.SetActive(false);
		Amount.gameObject.SetActive(false);
		Toggle.isOn = false;
	}
}
