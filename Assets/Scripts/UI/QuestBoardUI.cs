﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class QuestBoardUI : MonoBehaviour
{
	public GridLayoutGroup QuestsLayout;
	public GameObject QuestUITemplate;

	public void OnShow()
	{
		ArrayList currentQuests = GameManager.Instance.QuestController.Quests;

		ClearBoard();

		int questIndex = 0;
		foreach (QuestScript quest in currentQuests)
		{
			AddQuestToLayout(quest, questIndex);
			questIndex++;
		}
	}

	void ClearBoard()
	{
		for (int i = 0; i < QuestsLayout.transform.childCount; i++)
			Destroy(QuestsLayout.transform.GetChild(i).gameObject);
	}

	void AddQuestToLayout(QuestScript quest, int index)
	{
		GameObject questUI = Instantiate<GameObject>(QuestUITemplate);
		questUI.GetComponent<QuestUI>().Setup(quest);

		questUI.transform.SetParent(QuestsLayout.transform, false);
		
	}

	public void OnClose()
	{
		gameObject.SetActive(false);
	}
}
