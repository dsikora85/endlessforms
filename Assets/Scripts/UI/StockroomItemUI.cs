﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StockroomItemUI : MonoBehaviour
{
	public Stockroom.EFarmItem ItemType;
	public Image ImageNormal;
	public Image ImageLocked;
	//public bool Locked = false;

	Text amountText;

	void Start()
	{
		amountText = GetComponentInChildren<Text>();
	}


	public void OnShow()
	{
		amountText = GetComponentInChildren<Text>();
		StockFarmItem item = GameManager.Instance.Stockroom.GetStockItemByType(ItemType);

		amountText.text = item.Amount.ToString();

		ImageNormal.gameObject.SetActive(item.Unlocked);
		ImageLocked.gameObject.SetActive(!item.Unlocked);
	}

	void FillIconWithSingleColor(Image imageUI, Color color)
	{
		Color[] originalColorArray = imageUI.sprite.texture.GetPixels();

		Texture2D tex = new Texture2D(imageUI.sprite.texture.width, imageUI.sprite.texture.height, TextureFormat.RGBA32, false);
		Color fillColor = color;
		Color[] fillColorArray = tex.GetPixels();

		for (var i = 0; i < fillColorArray.Length; ++i)
		{
			if (originalColorArray[i].a > 0)
				fillColorArray[i] = fillColor;
			else
				fillColorArray[i] = originalColorArray[i];
		}

		tex.SetPixels(fillColorArray);
		tex.filterMode = imageUI.sprite.texture.filterMode;
		tex.wrapMode = imageUI.sprite.texture.wrapMode;
		tex.Apply();

		Sprite silhouette = Sprite.Create(tex, imageUI.sprite.rect, imageUI.sprite.pivot);
		imageUI.sprite = silhouette;
	}

}
