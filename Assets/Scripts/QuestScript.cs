﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SimpleJSON;

public class QuestScript : MonoBehaviour
{
	public enum QuestDifficulty
	{
		Easy,
		Medium,
		Hard
	}

	static readonly int MIN_QUEST_ITEMS_NUM_EASY = 1;
	static readonly int MAX_QUEST_ITEMS_NUM_EASY = 2;
	static readonly int MIN_QUEST_ITEMS_AMOUNT_EASY = 2;
	static readonly int MAX_QUEST_ITEMS_AMOUNT_EASY = 4;
	static readonly int MIN_QUEST_REWARD_EASY = 5;
	static readonly int MAX_QUEST_REWARD_EASY = 20;

	static readonly int MIN_QUEST_ITEMS_NUM_MID = 1;
	static readonly int MAX_QUEST_ITEMS_NUM_MID = 4;
	static readonly int MIN_QUEST_ITEMS_AMOUNT_MID = 3;
	static readonly int MAX_QUEST_ITEMS_AMOUNT_MID = 6;
	static readonly int MIN_QUEST_REWARD_MID = 10;
	static readonly int MAX_QUEST_REWARD_MID = 40;

	static readonly int MIN_QUEST_ITEMS_NUM_HARD = 2;
	static readonly int MAX_QUEST_ITEMS_NUM_HARD = 4;
	static readonly int MIN_QUEST_ITEMS_AMOUNT_HARD = 6;
	static readonly int MAX_QUEST_ITEMS_AMOUNT_HARD = 10;
	static readonly int MIN_QUEST_REWARD_HARD = 30;
	static readonly int MAX_QUEST_REWARD_HARD = 60;

	public class QuestItem
	{
		public Stockroom.EFarmItem ItemType;
		public int Amount;
		public int CoinsReward;

		public QuestItem(Stockroom.EFarmItem type, int amount)
		{
			ItemType = type;
			Amount = amount;
		}
	}
	public Dictionary<Stockroom.EFarmItem, QuestItem> QuestItems = new Dictionary<Stockroom.EFarmItem, QuestItem>();
	private int reward;
	public int Reward
	{
		get { return reward; }
	}

	public void Create(QuestDifficulty difficulty)
	{
		switch(difficulty)
		{
			case QuestDifficulty.Easy:
				SetupQuest(MIN_QUEST_ITEMS_NUM_EASY, MAX_QUEST_ITEMS_NUM_EASY, MIN_QUEST_ITEMS_AMOUNT_EASY, MAX_QUEST_ITEMS_AMOUNT_EASY, MIN_QUEST_REWARD_EASY, MAX_QUEST_REWARD_EASY, difficulty);
				break;
			case QuestDifficulty.Medium:
				SetupQuest(MIN_QUEST_ITEMS_NUM_MID, MAX_QUEST_ITEMS_NUM_MID, MIN_QUEST_ITEMS_AMOUNT_MID, MAX_QUEST_ITEMS_AMOUNT_MID, MIN_QUEST_REWARD_MID, MAX_QUEST_REWARD_MID, difficulty);
				break;
			case QuestDifficulty.Hard:
				SetupQuest(MIN_QUEST_ITEMS_NUM_HARD, MAX_QUEST_ITEMS_NUM_HARD, MIN_QUEST_ITEMS_AMOUNT_HARD, MAX_QUEST_ITEMS_AMOUNT_HARD, MIN_QUEST_REWARD_HARD, MAX_QUEST_REWARD_HARD, difficulty);
				break;
		}
	}

	void SetupQuest(int minItemsNum, int maxItemsNum, int minItemAmount, int maxItemAmount, int minReward, int maxReward, QuestDifficulty diff)
	{
		int itemsNum = UnityEngine.Random.Range(minItemsNum, maxItemsNum);
		for (int i = 0; i < itemsNum; i++)
		{
			Stockroom.EFarmItem itemID;
			if (diff == QuestDifficulty.Hard && i == 0)
				itemID = GameManager.Instance.Stockroom.GetLockedItem().ItemType;
			else
			{
				itemID = (diff == QuestDifficulty.Easy) ? GameManager.Instance.Stockroom.GetItemWithProbability().ItemType : GameManager.Instance.Stockroom.GetItemWithoutProbability().ItemType;
				while (QuestItems.ContainsKey(itemID))
					itemID = GameManager.Instance.Stockroom.GetItemWithProbability().ItemType;
			}
			int amount = UnityEngine.Random.Range(minItemAmount, maxItemAmount);

			QuestItem questItem = new QuestItem(itemID, amount);
			QuestItems.Add(itemID, questItem);
		}
		reward = UnityEngine.Random.Range(minReward, maxReward);
	}

	public bool IsCompleted()
	{
		bool completed = true;
		foreach (KeyValuePair<Stockroom.EFarmItem, QuestItem> questItem in QuestItems)
			if (GameManager.Instance.Stockroom.GetStockItemByType(questItem.Key).Amount < questItem.Value.Amount)
				completed = false;

		return completed;
	}

	public void Save(JSONClass json)
	{
		json["Quests"][-1]["Reward"].AsInt = reward;
		int index = json["Quests"].Count - 1;

		foreach (KeyValuePair<Stockroom.EFarmItem, QuestItem> questItem in QuestItems)
		{
			json["Quests"][index]["QuestItems"][-1]["type"].AsInt = (int)questItem.Key;
			int itemIndex = json["Quests"][index]["QuestItems"].Count - 1;
			json["Quests"][index]["QuestItems"][itemIndex]["amount"].AsInt = questItem.Value.Amount;
		}
	}

	public void Load(JSONNode json)
	{
		QuestItems.Clear();
		reward = json["Reward"].AsInt;
		JSONArray questItems = json["QuestItems"].AsArray;
		for(int i=0; i<questItems.Count; i++)
		{
			int amount = questItems[i]["amount"].AsInt;
			Stockroom.EFarmItem type = (Stockroom.EFarmItem)questItems[i]["type"].AsInt;
			QuestItem item = new QuestItem(type, amount);
			QuestItems.Add(type, item);
		}
	}
}
