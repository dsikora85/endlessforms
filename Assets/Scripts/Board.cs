﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Board : MonoBehaviour 
{
	public Vector2 Size = new Vector2(6, 6);
	public LayerMask BoardLayer;

	private static Board instance;

	public Tile[,] board;
	public int[,] test;

	private List<Tile> matchingTiles = new List<Tile>();
	private Tile currentCheckingTile;

	public HUDScript HUD;
	private BoardIndicator boardIndicator;

	public Transform StockroomPosition;

	public enum EBoardMode
	{
		normal,
		erasing
	}
	private EBoardMode boardMode = EBoardMode.normal;

	public bool IsInteractive
	{
		get { return boardMode == EBoardMode.normal; }
	}
	
	// Construct  
	private Board()
	{
		
	}

	//  Instance  
	public static Board Instance
	{
		get
		{
			if (instance == null)
				instance = GameObject.FindObjectOfType(typeof(Board)) as Board;
			return instance;
		}
	}

	// Use this for initialization
	void Start () 
	{
		board = new Tile[(int)Size.x, (int)Size.y];

		for (int x = 0; x < Size.x; x++)
			for (int y = 0; y < Size.y; y++)
			{
				Transform rowElemTransform = transform.Find("Row" + x);
				Transform elemTransform = rowElemTransform.Find("Elem0" + y);

				board[x, y] = elemTransform.GetComponent<Tile>();
				if (board[x, y])
					board[x, y].Reset(x, y);
				
			}

		if (PlayerPrefs.GetInt("SavedGame", 0) > 0)
			Load();
		else
		{
			for (int i = 0; i < 5; i++)
			{
				Tile tile;
				Transform elemTransform;
				do
				{
					int x = Random.Range(0, (int)Size.x);
					int y = Random.Range(0, (int)Size.x);
					Transform rowElemTransform = transform.Find("Row" + x);
					elemTransform = rowElemTransform.Find("Elem0" + y);
					tile = elemTransform.GetComponentInChildren<Tile>();
				}
				while (tile.IsBlocked);

				if (elemTransform)
				{
					tile.SetBlocked();
				}
			}
		}

		boardIndicator = GetComponent<BoardIndicator>();
	}
	
	public Tile GetTileOnPosition(Vector2 position)
	{
		RaycastHit2D hit = Physics2D.CircleCast(position, 0.1f, -Vector2.up, 0.2f, BoardLayer);
		if (hit)
		{
			Tile tile = hit.collider.GetComponent<Tile>();
			return tile;
		}

		return null;
	}

	public void SetEraseMode(bool val)
	{
		boardMode = val ? EBoardMode.erasing : EBoardMode.normal;

		if (val)
			GameManager.Instance.Spawner.Hide();
		else
			GameManager.Instance.Spawner.Show();
	}

	public void TryRemoveObject(Vector3 position)
	{
		if(boardMode == EBoardMode.erasing)
		{
			RaycastHit2D hit = Physics2D.CircleCast(position, 0.1f, -Vector2.up, 0.2f, BoardLayer);
			if(hit)
			{
				Tile tile = hit.collider.GetComponent<Tile>();
				if (!tile.IsEmpty || tile.IsBlocked)
				{
					//tile.Clear();
					HUD.ShowRemoveConfirmPopUp(()=> { tile.Clear(); });
				}
			}
		}
	}

	public bool TryPutObjectOnBoard(GameObject elem)
	{
		boardIndicator.HideIndicator();

		RaycastHit2D hit = Physics2D.CircleCast(elem.transform.position, 0.1f, -Vector2.up, 0.2f, BoardLayer);
		if (hit)
		{
			Tile tile = hit.collider.GetComponent<Tile>();
			if (!tile.IsEmpty || tile.IsBlocked)
				return false;

			FarmItem farmItem = elem.GetComponent<FarmItem>();
			tile.SetFarmItem(farmItem);
			farmItem.PutOnBoard();

			elem.transform.SetParent(hit.collider.gameObject.transform, true);
			elem.transform.localPosition = Vector3.zero;

			currentCheckingTile = tile;
			CheckMatch(tile);

			Save();

			GameManager.Instance.Spawner.Spawn();

			return true;
		}
		return false;
	}

	public void UpdateIndicator(GameObject elem)
	{
		RaycastHit2D hit = Physics2D.CircleCast(elem.transform.position, 0.1f, -Vector2.up, 0.2f, BoardLayer);
		if (hit)
		{
			Tile tile = hit.collider.GetComponent<Tile>();
			bool occupied = tile.IsBlocked || !tile.IsEmpty;
			boardIndicator.ShowIndicator(tile.Row, tile.Col, occupied);
		}
	}

	void CheckMatching(Tile tile)
	{
		if (tile.IsBlocked || tile.IsEmpty)
			return;

		if (tile.FarmItem.ItemType != currentCheckingTile.FarmItem.ItemType)
			return;

		if (matchingTiles.Contains(tile))
			return;

		matchingTiles.Add(tile);

		Tile tempTile;
		if (tile.Row > 0)
		{
			tempTile = board[tile.Row - 1, tile.Col];
			CheckMatching(tempTile);
		}

		if (tile.Row < Size.x - 1)
		{
			tempTile = board[tile.Row + 1, tile.Col];
			CheckMatching(tempTile);
		}

		if (tile.Col > 0)
		{
			tempTile = board[tile.Row, tile.Col - 1];
			CheckMatching(tempTile);
		}

		if (tile.Col < Size.y - 1)
		{
			tempTile = board[tile.Row, tile.Col + 1];
			CheckMatching(tempTile);
		}
	}

	void CheckMatch(Tile tile)
	{
		currentCheckingTile = tile;

		matchingTiles.Clear();

		CheckMatching(tile);

		if(matchingTiles.Count >= 3)
		{
			//TODO
			//add resource
			currentCheckingTile.FarmItem.HandleMatch(currentCheckingTile.Clear);

			foreach (Tile temp in matchingTiles)
			{
				if(temp != currentCheckingTile)
					temp.Clear();
			}
				
			matchingTiles.Clear();
		}
	}

	public void Load()
	{
		GameManager.Instance.Stockroom.Load();
		GameManager.Instance.QuestController.Load();
		GameManager.Instance.Spawner.Load();

		for (int x = 0; x < Size.x; x++)
			for (int y = 0; y < Size.y; y++)
			{
				int data = PlayerPrefs.GetInt("Board" + x + y, 0);
				//Debug.Log("load" + x + y + ": " + PlayerPrefs.GetInt("Board" + x + y, 0));
				board[x, y].LoadSet(data);
			}
	}

	public void Save()
	{
		PlayerPrefs.DeleteAll();

		PlayerPrefs.SetInt("SavedGame", 1);

		GameManager.Instance.Stockroom.Save();
		GameManager.Instance.QuestController.Save();
		GameManager.Instance.Spawner.Save();

		for (int x = 0; x < Size.x; x++)
			for (int y = 0; y < Size.y; y++)
			{
				PlayerPrefs.SetInt("Board" + x + y, board[x,y].SaveGet());
				//Debug.Log("save" + x + y + ": " + board[x, y].SaveGet());
			}
	}

	public void Reset()
	{
		PlayerPrefs.DeleteAll();

		for (int x = 0; x < Size.x; x++)
			for (int y = 0; y < Size.y; y++)
			{
				Transform rowElemTransform = transform.Find("Row" + x);
				Transform elemTransform = rowElemTransform.Find("Elem0" + y);

				board[x, y] = elemTransform.GetComponent<Tile>();
				if (board[x, y])
					board[x, y].Clear();

			}

		for (int i = 0; i < 5; i++)
		{
			Tile tile;
			Transform elemTransform;
			do
			{
				int x = Random.Range(0, (int)Size.x);
				int y = Random.Range(0, (int)Size.x);
				Transform rowElemTransform = transform.Find("Row" + x);
				elemTransform = rowElemTransform.Find("Elem0" + y);
				tile = elemTransform.GetComponentInChildren<Tile>();
			}
			while (tile.IsBlocked);

			if (elemTransform)
			{
				tile.SetBlocked();
			}
		}

		GameManager.Instance.Stockroom.Reset();
		GameManager.Instance.QuestController.Reset();
		GameManager.Instance.Spawner.Reset();
	}
}

