﻿using UnityEngine;

public class FormElementScript : MonoBehaviour 
{
	public GameObject[] Forms;

	private int currentLvl;
	public int CurrentLvl
	{
		get { return currentLvl; }
	}

	private float[] formProbability;

	private int MAX_FORMS; 

	// Use this for initialization
	void Start () 
	{
		MAX_FORMS = Forms.Length-1;

		formProbability = new float[MAX_FORMS];
		formProbability[0] = 0.5f;
		float maxProb = 0.5f;
		for(int i=1; i<formProbability.Length; i++)
		{
			formProbability[i] = formProbability[i - 1] / 2;
			maxProb += formProbability[i];
		}

		float random = Random.Range(0.0f, maxProb);
		for (int i = 0; i < MAX_FORMS; i++)
		{
			if (random < formProbability[i])
			{
				currentLvl = i;
				break;
			}

			random = random - formProbability[i];
		}
		
		GameObject elem = Instantiate(Forms[currentLvl], Vector3.zero, Quaternion.identity) as GameObject;
		elem.name = "form"+currentLvl;
		elem.transform.SetParent(transform, true);
		elem.transform.localPosition = Vector3.zero;

	   
	}
	
	public bool IsFinalForm()
	{
		return currentLvl == MAX_FORMS;
	}

	public GameObject GetFormWithLvl(int lvl)
	{
		return Forms[lvl];
	}

	public void Increment()
	{
		if(currentLvl < MAX_FORMS)
		{
			Debug.Log("increment");
			currentLvl++;
			int children = transform.childCount;
			for (int i = 0; i < children; ++i)
			{
				Debug.Log("destroy");
				Destroy(transform.GetChild(i).gameObject);
			}

			GameObject elem = Instantiate(Forms[currentLvl], Vector3.zero, Quaternion.identity) as GameObject;
			elem.name = "form"+currentLvl;
			elem.transform.SetParent(transform, true);
			elem.transform.localPosition = Vector3.zero;
		}
	}
}
