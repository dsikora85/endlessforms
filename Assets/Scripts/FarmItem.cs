﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System;

public class FarmItem : MonoBehaviour
{
	[SerializeField]
	private SpriteRenderer boardImage;
	[SerializeField]
	private SpriteRenderer spawnerImage;
	[SerializeField]
	private SpriteRenderer Icon;

	[SerializeField]
	private Stockroom.EFarmItem itemType;
	public Stockroom.EFarmItem ItemType
	{
		get { return itemType; }
	}

	[SerializeField]
	bool Unlocked = false;

	Action onFinishCallback;

	public void Start()
	{

	}

	public void Spawn()
	{
		boardImage.gameObject.SetActive(false);
		Icon.gameObject.SetActive(false);
		spawnerImage.gameObject.SetActive(true);
	}

	public void Show()
	{
		spawnerImage.gameObject.SetActive(true);
	}

	public void Hide()
	{
		spawnerImage.gameObject.SetActive(false);
	}

	public void PutOnBoard()
	{
		spawnerImage.gameObject.SetActive(false);
		Icon.gameObject.SetActive(false);
		boardImage.gameObject.SetActive(true);

		CircleCollider2D collider = GetComponent<CircleCollider2D>();
		collider.enabled = false;
	}

	public void HandleMatch(Action callback)
	{
		GameManager.Instance.Stockroom.AddItem(ItemType, 1);
		Icon.gameObject.SetActive(true);
		boardImage.gameObject.SetActive(false);
		StartCoroutine(PickUpItemAnimation());

		onFinishCallback = callback;
	}

	IEnumerator PickUpItemAnimation()
	{
		yield return DOTween.To(() => transform.position, (newPos) => transform.position = newPos, Board.Instance.StockroomPosition.position, 0.5f).WaitForCompletion();

		onFinishCallback();
		Destroy(gameObject);
	}

	public int SaveGet()
	{
		return (int)itemType;
	}

	public void LoadSet(int type)
	{
		Stockroom.EFarmItem tt = (Stockroom.EFarmItem)type;
	}
}
