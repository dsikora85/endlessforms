﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class Stockroom : MonoBehaviour
{
	public enum EFarmItem
	{
		Tomato,
		Carrot,
		Corn,
		Radish,
		Potato,
		Eggplant,
		Pumpkin,
		CauliFlower,
		Grapes,
		Bamboo,
		Leek,
		Wheat,
		Cabbage,
		Mushroom,
		BlockedStub1,
		BlockedStub2,
		BlockedHay1,
		BlockedHay2,
		BlockedCrate
	}

	public StockFarmItem[] StockItems;
	public GameObject[] Blocked;
	public int StartCoinsAmount = 100;
	private Dictionary<EFarmItem, FarmItem> farmItemsDictionary;

	private int coinsAmount;
	public int CoinsAmount
	{
		get { return coinsAmount; }
	}

	void Start()
	{
		coinsAmount = StartCoinsAmount;
		farmItemsDictionary = new Dictionary<EFarmItem, FarmItem>();

		foreach (StockFarmItem stockItem in StockItems)
			farmItemsDictionary.Add(stockItem.FarmItem.ItemType, stockItem.FarmItem);
	}

	public void AddCoins(int amount)
	{
		coinsAmount += amount;
	}

	public void SpendCoins(int amount)
	{
		coinsAmount -= amount;
	}

	public FarmItem GetItemWithProbability()
	{
		float total = 0;
		StockFarmItem[] availableItems = StockItems.Where((elem) => elem.Unlocked == true).ToArray();

		foreach (StockFarmItem elem in availableItems)
		{
			total += elem.Probability;
		}

		float randomPoint = UnityEngine.Random.value * total;

		for (int i = 0; i < availableItems.Length; i++)
		{
			if (randomPoint < availableItems[i].Probability)
			{
				return availableItems[i].FarmItem;
			}
			else
			{
				randomPoint -= availableItems[i].Probability;
			}
		}
		return availableItems[0].FarmItem;
	}

	public FarmItem GetItemWithoutProbability()
	{
		StockFarmItem[] availableItems = StockItems.Where((elem) => elem.Unlocked == true).ToArray();
		int itemID = UnityEngine.Random.Range(0, availableItems.Length);
		return availableItems[itemID].FarmItem;
	}

	public FarmItem GetLockedItem()
	{
		StockFarmItem[] lockedItems = StockItems.Where((elem) => elem.Unlocked == false).ToArray();
		int itemID = UnityEngine.Random.Range(0, lockedItems.Length);
		return lockedItems[itemID].FarmItem;
	}

	public StockFarmItem GetStockItemByType(EFarmItem type)
	{
		foreach(StockFarmItem item in StockItems)
		{
			if (item.FarmItem.ItemType == type)
				return item;
		}

		return null;
	}

	public FarmItem GetItemByType(EFarmItem type)
	{
		StockFarmItem stockFarmItem = StockItems.Where(elem => elem.FarmItem.ItemType == type).ToArray()[0];
		FarmItem farmItem = Instantiate<FarmItem>(stockFarmItem.FarmItem);
		//farmItem.StockFarmItem = stockFarmItem;
		return farmItem;
	}

	public void AddItem(EFarmItem type, int amount)
	{
		StockFarmItem stockFarmItem = StockItems.Where(elem => elem.FarmItem.ItemType == type).ToArray()[0];
		stockFarmItem.IncreaseAmount(amount);
	}

	public void Save()
	{
		PlayerPrefs.SetInt("Coins", coinsAmount);
		foreach (StockFarmItem stockFarmItem in StockItems)
			stockFarmItem.Save();
	}

	public void Load()
	{
		PlayerPrefs.GetInt("Coins", 0);
		foreach (StockFarmItem stockFarmItem in StockItems)
			stockFarmItem.Load();
	}

	public void Reset()
	{
		coinsAmount = StartCoinsAmount;

		foreach (StockFarmItem stockFarmItem in StockItems)
			stockFarmItem.Reset();
	}

	public void UnlockLevel1Items()
	{
		GetStockItemByType(EFarmItem.Pumpkin).Unlocked = true;
		GetStockItemByType(EFarmItem.CauliFlower).Unlocked = true;
		GetStockItemByType(EFarmItem.Mushroom).Unlocked = true;
		GetStockItemByType(EFarmItem.Bamboo).Unlocked = true;
	}

	public void UnlockLevel2Items()
	{
		GetStockItemByType(EFarmItem.Leek).Unlocked = true;
		GetStockItemByType(EFarmItem.Wheat).Unlocked = true;
		GetStockItemByType(EFarmItem.Cabbage).Unlocked = true;
		GetStockItemByType(EFarmItem.Grapes).Unlocked = true;
	}
}
