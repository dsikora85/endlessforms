﻿using UnityEngine;
using UnityEditor;
using System.Collections;

class EditorTools : EditorWindow
{
	[MenuItem("Tools/Clear Player Prefs")]
	public static void ClearData()
	{
		PlayerPrefs.DeleteAll();
	}

}
